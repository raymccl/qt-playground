#include "serviceprovider.h"

ServiceProvider::ServiceProvider()
{
    this->fileService = new FileService();
    this->logService = new FileLogService();
    this->networkService = new NetworkService();
}

FileService *ServiceProvider::getFileService() const
{
    return this->fileService;
}

NetworkService *ServiceProvider::getNetworkService() const
{
    return this->networkService;
}

LogService *ServiceProvider::getLogService() const
{
    return this->logService;
}
