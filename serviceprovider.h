#ifndef SERVICEPROVIDER_H
#define SERVICEPROVIDER_H

#include "services/fileservice.h"
#include "services/logservice.h"
#include "services/filelogservice.h"
#include "services/networkservice.h"

class ServiceProvider
{
public:
    ServiceProvider();
    FileService *getFileService() const;
    NetworkService *getNetworkService() const;
    LogService *getLogService() const;

private:
    LogService * logService;
    FileService * fileService;
    NetworkService * networkService;
};

#endif // SERVICEPROVIDER_H
