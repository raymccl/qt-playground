#include "fileservice.h"

FileService::FileService()
{

}

QString FileService::read(QString filePath) {
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
       return "";

   QTextStream in(&file);
   QString val;
   while (!in.atEnd()) {
       QString line = in.readLine();
       val += line;
   }
   return val;
}

void FileService::writeLine(QString filePath, QString content)
{
    QFile file(filePath);

    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QTextStream out(&file);
        out << content << endl;
        return;
    } else {
        qDebug() << "Failure";
    }
}
