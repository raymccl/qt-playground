#ifndef FILELOGSERVICE_H
#define FILELOGSERVICE_H

#include "logservice.h"
#include "serviceprovider.h"
#include <QDir>

class FileLogService : public LogService
{
public:
    FileLogService();
    /**
     * @brief Appends a DEBUG message to the log file.
     * @param message The message to log.
     */
    void debug(QString message);
    /**
     * @brief Appends an INFO message to the log file.
     * @param message The message to log.
     */
    void info(QString message);
    /**
     * @brief Appends a WARN message to the log file.
     * @param message The message to log.
     */
    void warn(QString message);
    /**
     * @brief Appends an ERROR message to the log file.
     * @param message The message to log.
     */
    void error(QString message);
    /**
     * @brief Appends a FATAL message to the log file.
     * @param message The message to log.
     */
    void fatal(QString message);
private:
    /**
     * @brief Boolean to determine whether logs should be written to the console.
     */
    bool consoleOut = true;

    /**
     * @brief Returns the full path to the log file.
     * @return The full path to the log file.
     */
    QString getLogFilePath ();
    /**
     * @brief Appends the log message preceeded by the current time and log type to the log file.
     * @param level The log level of the provided message.
     * @param type The log type of the provided message.
     * @param message The message to append to the log file.
     */
    void log(LogLevel level, QString type, QString message);
};

#endif // FILELOGSERVICE_H
