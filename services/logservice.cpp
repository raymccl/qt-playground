#include "logservice.h"

void LogService::debug(QString message)
{
    log(LogLevel::DEBUG, "DEBUG", message);
}

void LogService::info(QString message)
{
    log(LogLevel::INFO, "INFO", message);
}

void LogService::warn(QString message)
{
    log(LogLevel::WARN, "WARN", message);
}

void LogService::error(QString message)
{
    log(LogLevel::ERROR, "ERROR", message);
}

void LogService::fatal(QString message)
{
   log(LogLevel::FATAL, "FATAL", message);
}

QString LogService::getFullMessage(QString type, QString message)
{
    return QDateTime::currentDateTimeUtc().toString() + " - " + type + " : " + message;
}

void LogService::log(LogService::LogLevel level, QString type, QString message)
{
    if (this->logLevel >= level) {
        qDebug() << getFullMessage(type, message);
    }
}


