#ifndef FILE_H
#define FILE_H

#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QByteArray>
#include <QDebug>

class FileService
{
public:
    FileService();

    /**
     * @brief Reads the content of the provided file and returns it as a string.
     * @param filePath The path of the file to read.
     * @return The content of the file as a string.
     */
    QString read(QString filePath);

    /**
     * @brief Appends the content to the last line of the file.
     * @param filePath The path of the file to write to.
     * @param content The content to append.
     */
    void writeLine(QString filePath, QString content);
};

#endif // FILE_H
