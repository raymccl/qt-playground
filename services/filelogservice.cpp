#include "filelogservice.h"

FileLogService::FileLogService()
{

}

void FileLogService::debug(QString message)
{
    log(LogLevel::DEBUG, "DEBUG", message);
}

void FileLogService::info(QString message)
{
    log(LogLevel::INFO, "INFO", message);
}

void FileLogService::warn(QString message)
{
    log(LogLevel::WARN, "WARN", message);
}

void FileLogService::error(QString message)
{
    log(LogLevel::ERROR, "ERROR", message);
}

void FileLogService::fatal(QString message)
{
    log(LogLevel::FATAL, "FATAL", message);
}

void FileLogService::log(LogLevel level, QString type, QString message)
{
    if (this->logLevel >= level) {
        ServiceProvider provider;
        FileService fileService = *provider.getFileService();

        QString fullMessage = getFullMessage(message, type);

        fileService.writeLine(getLogFilePath(), fullMessage);

        if (this->consoleOut) {
            qDebug() << fullMessage;
        }
    }
}

QString FileLogService::getLogFilePath()
{
    return QDir::homePath() + "/logFile.txt";
}
