#ifndef LOGGER_H
#define LOGGER_H

#include <QString>
#include <QDebug>
#include <QDateTime>

class LogService
{
public:
    /**
     * @brief Logs an DEBUG message.
     * @param message The message to log.
     */
    virtual void debug(QString message);
    /**
     * @brief Logs an INFO message.
     * @param message
     */
    virtual void info(QString message);
    /**
     * @brief Logs a WARN message.
     * @param message The message to log.
     */
    virtual void warn(QString message);
    /**
     * @brief Logs an ERROR message.
     * @param message The message to log.
     */
    virtual void error(QString message);
    /**
     * @brief Logs a FATAL message.
     * @param message The message to log.
     */
    virtual void fatal(QString message);

    enum LogLevel {
        DEBUG,
        INFO,
        WARN,
        ERROR,
        FATAL
    };
protected:
    /**
     * @brief Combines the current time, log type, and message and returns the full message.
     * @param type The type of log.
     * @param message The message to log.
     * @return The full log message.
     */
    QString getFullMessage (QString type, QString message);
    /**
     * @brief The minimum level for logs.
     */
    LogLevel logLevel = LogLevel::FATAL;
private:
    void log(LogLevel level, QString type, QString message);
};

#endif // LOGGER_H
