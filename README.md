# qt-playground

Playground for QT and C++ implementations and C++ best practices.

## Commits

Commits follow the conventional commits format to enable changelog from the commit history.

Conventional Commit nouns are as follows:

- feat: A commit which adds a new feature to the application.
- fix: A commit which represents a bugfix.
- docs: A commit which updates the documentation of the library.
- ci: A commit which updates the CI/CD pipeline of the library.