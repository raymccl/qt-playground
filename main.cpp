#include "mainwindow.h"

#include <QApplication>
#include "serviceprovider.h"
#include <QDebug>

/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ServiceProvider provider;

    LogService *log = provider.getLogService();

    log->debug("Log Test");

    MainWindow w;

    w.show();
    return a.exec();
}
